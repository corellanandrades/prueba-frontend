# GPS Global  (prueba-frontend)

Prueba Frontend Desarrollador de software

## Instrucciones

Bienvenido a la prueba frontend para el cargo de Desarrollador
de software en GPS Global. El objetivo de esta prueba es conocer
tus habilidades de desarrolo en un proyecto con vue.js
con quasar framework.

Para desarrollar esta prueba debes seguir las siguientes intrucciones de instalación del proyecto.

### Instalar Quasar Framework globalmente
```bash
sudo npm install -g @quasar/cli
```

### Instalar Dependencias
```bash
npm install
```

### Iniciar App en modo desarrollo
```bash
npm run dev
```

### Documentación del framework
Cualquier duda sobre usar e implementar componentes visuales ver en [documentación quasar framework](https://quasar.dev).


# Desarrollos propuestos

Una vez con el proyecto operando en tu navegador, se te asignaron las siguientes tareas que debes
implementar en esta aplicación, las cuales tomamos en cuenta para analizar tus habilidades,
por lo cuál no hay una forma concreta de hacerlo así que lo dejamos a tu criterio.

Las tareas que debes crear en esta aplicación son las siguientes:

- Consumir datos de proyecto prueba-backend (enlace: http://localhost:xxxx/api/recorrido) (ejemplo: con axios o fetch api).
- Mostrar datos del recorrido en tabla 'Resultado Recorrido' en Index.vue
  (Si investigas en la documentación de quasar encontrarás ayuda para manipular la tabla,
  entre más completa y elaborada la tabla mejor.)

Puntos extra:

- Mostrar marcadores en el mapa, guiarse en la estructura del array marcadores de Index.vue para crear el arreglo de marcadores en base al recorrido que obtuviste.

# Envío del proyecto

Este proyecto debe ser resuelto y comprimido junto con proyecto prueba-backend en un archivo .zip

Enviar el archivo con asunto Prueba de desarrollo de software a
informatica@gpsglobal.cl

Mucho éxito en este desafio, cualquier consulta preguntar al mismo correo.
